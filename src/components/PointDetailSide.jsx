import React, { Fragment, useEffect } from 'react';
import {
    IconButton,
    ListItem, 
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText, 
    TextField,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import { useSelector, useDispatch } from 'react-redux';
import { 
    deletePoint, 
    getPointFiles,
    setActivePoint,
    updatePoint, 
} from '../redux/travelDuck';
import { useForm } from "react-hook-form";
import { setShowMarkerDrawer } from '../redux/coreDuck';
import ConfirmationDialog from './ConfirmationDialog';
import FileGallery from './FileGallery';


const PointDetailSide = ({ point }) => {

    const [deleteDialogOpen, setDeleteDialogOpen] = React.useState(false);

    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();

    const activePointFiles = useSelector(store => store.travel.activePointFiles);

    useEffect(() => {
        dispatch(getPointFiles(point));
    }, [dispatch, point]);

    const onSubmit = data => {
        dispatch(updatePoint(point, data));
    }


    const onDeleteDialogResult = async action => {
        if (action) {
            dispatch(setShowMarkerDrawer(false));
            dispatch(deletePoint(point));
            dispatch(setActivePoint(null));
        }
        setDeleteDialogOpen(false);
    };

    const handleDeleteClick = () => {
        setDeleteDialogOpen(true);
    }

    return (
        <Fragment>
            <form onSubmit={handleSubmit(onSubmit)}>
            <ListItem>
                <ListItemText secondary={point.properties.description} />
            </ListItem>
            <ListItem>
                <TextField
                    label="Notes"
                    name="notes"
                    multiline
                    rows={4}
                    defaultValue={point.properties.notes}
                    variant="outlined"
                    inputRef={register}
                    fullWidth
                />
            </ListItem>
            <ListItem>
                <FileGallery point={point} files={activePointFiles}/>
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <IconButton edge="start" aria-label="save" type="submit">
                        <SaveIcon />
                    </IconButton>
                </ListItemIcon>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete" onClick={handleDeleteClick}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            </form>
            <ConfirmationDialog
                onClose={onDeleteDialogResult}
                open={deleteDialogOpen}
                title="Delete?"
                content="Seguro pa?"
            />
        </Fragment>
    );
}

export default PointDetailSide;