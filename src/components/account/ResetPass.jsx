import React, { Fragment } from 'react';
import { 
    Avatar,
    Button,
    Grid,
    TextField,
    Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import VpnKeyIcon from '@material-ui/icons/VpnKey';

const ResetPass = (props) => {

    const { classes } = props;

    return (
        <Fragment>
            <Avatar className={classes.avatar}>
                <VpnKeyIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Reset password
            </Typography>
            <form className={classes.form} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Reset password
                </Button>
                <Grid container>
                    <Grid item xs>
                        <Link to="/account/login" variant="body2">
                        {"Do you already have an account? Sign In"}
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link to="/account/signup" variant="body2">
                        {"Don't have an account? Sign Up"}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </Fragment>
    )
}

export default ResetPass;