import React from 'react';
import { 
    Box,
    CssBaseline,
    Grid,
    Link,
    Paper,
    Typography,
} from '@material-ui/core';
import { Route, Redirect } from 'react-router-dom';
import SingIn from './SingIn'
import SingUp from './SingUp'
import ResetPass from './ResetPass'
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://google.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInSide() {
  const classes = useStyles();

  const token = useSelector(store => store.user.token);

  return (
    token ? (
      <Redirect to="/travel" />
    ) :
    (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
              <Route path="/account/login">
                  <SingIn classes={classes}/>
              </Route>
              <Route path="/account/signup">
                  <SingUp classes={classes}/>
              </Route>
              <Route path="/account/reset">
                  <ResetPass classes={classes}/>
              </Route>
              <Redirect from="/account" to="/account/login" />
              
          </div>
          <Box mt={5}>
              <Copyright />
          </Box>
        </Grid>
      </Grid>
    )
  );
}