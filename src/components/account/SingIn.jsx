import React, { Fragment } from 'react';
import { 
    Avatar,
    Button,
    Checkbox,
    FormControlLabel,
    Grid,
    TextField,
    Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { signIn } from '../../redux/userDuck';

const SingIn = (props) => {

    const { classes } = props;

    const { register, errors, handleSubmit } = useForm();

    const [errorMessage, setErrorMessage] = React.useState([]);

    const dispatch = useDispatch();

    const onSubmit = data => {
        setErrorMessage([])
        let resStatus = 0;
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        };
        fetch('http://localhost:8000/token-auth/', requestOptions)
        .then(response => {
            resStatus = response.status
            return response.json()
        })
        .then(response => {
            switch (resStatus) {
                case 200:
                    localStorage.setItem('token', response.token);
                    dispatch(signIn(response.token));
                    break
                case 400:
                    console.log(response)
                    let errors = [];
                    for (var k in response) {
                        if (response[k].length > 0){
                            errors.push(response[k][0])
                        }
                        //TODO: intentar usar 'setError' de react hook forms para poner el error en cada input
                    }
                    setErrorMessage(errors)
                    break
                case 500:
                    setErrorMessage(['Server error, try again'])
                    break
                default:
                    setErrorMessage(['Unknown error, try again'])
                    break
            }
        })
        .catch(err => {
            console.error(err)
        })
        
    }

    return (
        <Fragment>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            {
                errorMessage.length > 0 &&
                errorMessage.map(message => 
                        <Typography key={message} align='left' color='error' component="h5">
                            {message}
                        </Typography>
                )
            }
            <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
                <Grid container>
                    <Grid item xs={false} sm={12} md={12}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            inputRef={register({ required: "Username is required" })}
                            fullWidth
                            label="Username"
                            name="username"
                            autoFocus
                        />
                        {errors.username && errors.username.message}
                    </Grid>
                    <Grid item xs={false} sm={12} md={12}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            inputRef={register({ required: "Password is required" })}
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                        />
                        {errors.password && errors.password.message}
                    </Grid>
                </Grid>
                { /* TODO: este check no hace nada aun */}
                <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label="Remember me"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Sign In
                </Button>
                <Grid container>
                    <Grid item xs>
                        <Link to="/account/reset" variant="body2">
                        Forgot password?
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link to="/account/signup" variant="body2">
                        {"Don't have an account? Sign Up"}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </Fragment>
    )
}

export default SingIn;