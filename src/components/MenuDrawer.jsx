import React from 'react'
import { 
    makeStyles,
    Drawer,
    Divider
} from '@material-ui/core'
import {
    List, 
    ListItem, 
    ListItemIcon
} from '@material-ui/core'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import { useDispatch } from 'react-redux';
import { setShowDrawer } from '../redux/coreDuck';
const drawerWidth = 240
const styles = makeStyles(theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    listDrawer: {
        paddingTop: 0
    }
}))
const MenuDrawer = (props) => {

    const classes = styles();

    const dispatch = useDispatch();

    //TODO: https://material-ui.com/es/components/dialogs/#full-screen-dialogs
    //para mostar perfil y esas cosas
    return (
        <Drawer 
            className={classes.drawer}
            variant={props.variant}
            classes={{
                paper: classes.drawerPaper
            }} 
            open={props.open}
            onClose={() => props.onClose()}
            anchor="left"
        >
            <List component="nav" className={classes.listDrawer}>
                <ListItem button className={classes.toolbar}>
                    <ListItemIcon onClick={() => dispatch(setShowDrawer(false))}>
                        <DoubleArrowIcon />
                    </ListItemIcon>
                </ListItem>
                <Divider />
            </List>
        </Drawer>
    );
}

export default MenuDrawer;