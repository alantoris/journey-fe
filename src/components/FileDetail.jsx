import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    ButtonBase,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    deleteMeetingClose: {
        fontSize: '1.5em',
        cursor: 'pointer',
        position: 'absolute',
        right: '10px',
        top: '5px',
    }
}));
  

const FileDetail = ({open, file, onClose}) => {

    const classes = useStyles();

    return (
            <Dialog
                maxWidth={"xl"}
                open={open}
                onClose={onClose}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle id="max-width-dialog-title">
                    {file.name}
                    <ButtonBase 
                        className={classes.deleteMeetingClose} 
                        onClick={onClose}
                    >
                        <span >&times;</span>
                    </ButtonBase>    
                </DialogTitle>
                <DialogContent>
                    {
                        //TODO: Agregar el contenido del archivo aca
                        //HACER UN GET A FILES
                    }
                </DialogContent>
                <DialogActions>
                    {
                        //TODO: Agregar botones para descargar y borrar el archivo
                    }
                </DialogActions>
            </Dialog>
    );
}

export default FileDetail;