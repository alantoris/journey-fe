import React from 'react'
import { 
    makeStyles,
    Drawer,
    Divider
} from '@material-ui/core'
import {
    List, ListItem, ListItemIcon, ListItemText
} from '@material-ui/core'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import PointSideList from './PointSideList';
import { useDispatch } from 'react-redux';
import { setShowDrawer } from '../redux/coreDuck';
const drawerWidth = 240
const styles = makeStyles(theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    listDrawer: {
        paddingTop: 0
    }
}))

//DEPRECATED - TODO: Borrar?
const SideDrawer = (props) => {

    const classes = styles();

    const dispatch = useDispatch();


    return (
        <Drawer 
            className={classes.drawer}
            variant={props.variant}
            classes={{
                paper: classes.drawerPaper
            }} 
            open={props.open}
            onClose={() => props.onClose()}
            anchor="right"
        >
            <List component="nav" className={classes.listDrawer}>
                <ListItem button className={classes.toolbar}>
                    <ListItemIcon onClick={() => dispatch(setShowDrawer(false))}>
                        <DoubleArrowIcon />
                    </ListItemIcon>
                    <ListItemText primary="List1"/>
                </ListItem>
                <Divider />
                <PointSideList />
            </List>
            <Divider />
        </Drawer>
    );
}

export default SideDrawer;