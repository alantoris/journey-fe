import React from 'react'
import { 
    AppBar,
    IconButton,  
    Menu, 
    MenuItem, 
    Toolbar,
    Typography, 
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu'
import PersonIcon from '@material-ui/icons/Person';
import { logOut } from '../redux/userDuck';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title:{
        flexGrow: 1,
        textAlign: "center",
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}))

const Navbar = (props) => {

    const classes = useStyles();

    const dispatch = useDispatch();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const name = useSelector(store => store.travel.name);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    const handleLogOut = () => {
        dispatch(logOut());
    }

    return(
        <AppBar className={classes.appBar}>
            <Toolbar>
                <IconButton 
                    color="inherit" 
                    className={classes.menuButton} 
                    aria-label="menu"
                    onClick={() => props.menuIconAction()}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                Map: {name}
                </Typography>   
                <IconButton aria-label="account" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                    <PersonIcon />
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={handleClose}>My account</MenuItem>
                    <MenuItem onClick={handleLogOut}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
    );
}

export default Navbar;