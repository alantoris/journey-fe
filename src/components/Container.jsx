import React, { useEffect } from 'react';
import {
    Hidden,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Navbar from './Navbar';
import MenuDrawer from './MenuDrawer';
import MarkerSideDrawer from './MarkerSideDrawer';
import JoruneyMap from './JoruneyMap';
import ActionButtons from './ActionButtons';
import ManageTravelDialog from './ManageTravelDialog'
import { setShowDrawer, setShowMarkerDrawer } from '../redux/coreDuck';
import { 
    clearActivePointFiles, 
    getAvailablesTravels, 
    getTravelFeatures 
} from '../redux/travelDuck';
import { getUserData } from '../redux/userDuck';


const estilos = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        height: theme.mixins.toolbar.minHeight
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },
    mapHeight: {
        width: "100%",
        height: `calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
    }
}))


const Container = (props) => {

    const classes = estilos();

    const dispatch = useDispatch();

    const token = useSelector(store => store.user.token);
    const activeTravelUUID = useSelector(store => store.travel.travelUUID);

    useEffect(() => {
        dispatch(getUserData());
        if (activeTravelUUID === null) {
            dispatch(getAvailablesTravels());
        }
        else{
            dispatch(getTravelFeatures(activeTravelUUID));
        }
    }, [activeTravelUUID, dispatch]);

    const showDrawer = useSelector(store => store.core.showDrawer);
    const showMarkerDrawer = useSelector(store => store.core.showMarkerDrawer);

    const openDrawer = () => {
        dispatch(setShowDrawer(!showDrawer))
    }

    const openMarkerDrawer = () => {
        dispatch(setShowMarkerDrawer(!showDrawer))
    }

    const handleCloseMarkerSideDrawer = () => {
        openMarkerDrawer();
        dispatch(clearActivePointFiles());
    }

    return (
        token ? (
            <div className={classes.root}>
                <Navbar menuIconAction={openDrawer}/>
                <Hidden>
                    <MenuDrawer variant="temporary" open={showDrawer} onClose={() => openDrawer(false)}/>
                </Hidden>
                <Hidden>
                    <MarkerSideDrawer variant="temporary" open={showMarkerDrawer} onClose={() => handleCloseMarkerSideDrawer}/>
                </Hidden>
                <div className={classes.content}>
                    <div className={classes.toolbar}></div>
                    <JoruneyMap classes={classes}/>
                </div>
                <ActionButtons />
                <ManageTravelDialog />
            </div>
        ) : (
            <Redirect to="/account/login" />
        )

    )
}

export default Container;