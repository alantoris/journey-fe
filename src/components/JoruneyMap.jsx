import React from 'react'
import { 
  Map as LeafletMap, 
  FeatureGroup,
  TileLayer, 
  Marker, 
  Polyline
} from 'react-leaflet'
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import {  useSelector, useDispatch } from 'react-redux';
import AddPointDialog from './AddPointDialog';
import MarkerPopup from './MarkerPopup';
import { setActivePoint, setNewCoordinates } from '../redux/travelDuck';
import { ADDING_POINT, setShowMarkerDrawer } from '../redux/coreDuck'


const JoruneyMap = (props) => {

    const [addPointDialogOpen, setAddPointDialogOpen] = React.useState(false);

    const dispatch = useDispatch();

    const travel = useSelector(store => store.travel);
    const activePoint = useSelector(store => store.travel.activePoint);
    const mapCoordinates = useSelector(store => store.travel.mapCoordinates);
    const currentAction = useSelector(store => store.core.currentAction);
    const mapInitialData = useSelector(store => store.core.mapInitialData);

    const { classes } = props;

    const handleOnClick = (item) => {
      dispatch(setActivePoint(item))
    }
    
    /* THE CURSOR COORDINATES */
    /*const handleCursor = (e) => {
      console.log([e.latlng.lng, e.latlng.lat]);
    }*/

    const handleMapClick = (event) => {
      dispatch(setNewCoordinates(event.latlng))
      if (currentAction === ADDING_POINT){ 
        setAddPointDialogOpen(true); 
      }
    }

    const handleMarkerDetails = () => {
      dispatch(setShowMarkerDrawer(true))
    }

    return (
      <LeafletMap 
        center={[mapCoordinates[1], mapCoordinates[0]]} 
        className={classes.mapHeight}
        zoom={mapInitialData.zoom}
        onClick={handleMapClick}
        maxZoom={mapInitialData.maxZoom}
        minZoom={mapInitialData.minZoom}
        maxBounds={mapInitialData.bounds}
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          zIndex={-1} 
        />
        <FeatureGroup>
        {travel.features.pointsArray.map(point => (
          <Marker 
            key={point.id} 
            position={[
              point.geometry.coordinates[1],
              point.geometry.coordinates[0]
            ]}
            icon={L.icon({
              iconUrl: 'location-outline.svg',
              iconSize: [38, 38],
              iconAnchor: [22, 37],
            })}
            onclick={() => {
              handleOnClick(point);
            }}
          />
        ))}
        {
          travel.features.pathsArray.map(path => (
            <Polyline 
              key={path.id} 
              positions={
                [
                  [path.geometry.coordinates[1][1],path.geometry.coordinates[1][0]],
                  [path.geometry.coordinates[0][1],path.geometry.coordinates[0][0]]
                ]
              } 
              color={'red'} 
            />
          ))
        }

        {activePoint && (<MarkerPopup point={activePoint} handleMarkerDetails={handleMarkerDetails}/>)}
          <AddPointDialog open={addPointDialogOpen} handleOnClose={() => setAddPointDialogOpen(false)}/>
        </FeatureGroup>
      </LeafletMap>
    )
}

export default JoruneyMap;