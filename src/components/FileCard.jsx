import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    ButtonBase,
    Card,
    CardActions,
    CardContent,
    IconButton,
    Typography,
} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import DeleteIcon from '@material-ui/icons/Delete';
import DescriptionIcon from '@material-ui/icons/Description';
import FileDetail from './FileDetail';

const useStyles = makeStyles({
    root: {
      minWidth: 110,
      minHeight: 158,
    },

    title: {
      fontSize: 14,
    },
});

const FileCard = ({file}) => {
    const classes = useStyles();

    const [openDetail, setOpenDetail] = React.useState(false);

    const handleClickOpenDetail = () => {
        console.log("abrir")
        setOpenDetail(true);
    };

    const handleCloseDetail = () => {
        setOpenDetail(false);
    };

    return (
        <React.Fragment>
            <Card className={classes.root}>
                <ButtonBase
                    onClick={handleClickOpenDetail}
                >
                    <CardContent>
                        <DescriptionIcon fontSize="large"/>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            {file.name}
                        </Typography>
                    </CardContent>
                </ButtonBase>
                <CardActions>
                    <IconButton aria-label="details">
                        <GetAppIcon fontSize="small"/>
                    </IconButton>
                    <IconButton aria-label="details">
                        <DeleteIcon fontSize="small"/>
                    </IconButton>
                </CardActions>
            </Card>
            <FileDetail open={openDetail} file={file} onClose={handleCloseDetail}/>
        </React.Fragment>
    );
}

export default FileCard;