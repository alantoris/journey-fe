import React from 'react';
import { 
    Button,
    Dialog, 
    DialogTitle, 
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField, 
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import {  useSelector, useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { setCurrentAction, MANAGING_MAP } from '../redux/coreDuck'
import { createTravel, getAvailablesTravels, selectTravel } from '../redux/travelDuck';


const styles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}))

const ManageTravelDialog = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();

    const classes = styles();

    const currentAction = useSelector(store => store.core.currentAction);
    const availablesTravels = useSelector(store => store.travel.availablesTravels);

    const onSubmit = data => {
        dispatch(createTravel(data));
        dispatch(setCurrentAction(null));
        //Setear como mapa activo al nuevo
        dispatch(getAvailablesTravels());
    }
    
    const handleSelectTravel = (event) => {
        dispatch(selectTravel(event.target.value))
        dispatch(setCurrentAction(null));
    }

    return (
        <Dialog 
            onClose={() => dispatch(setCurrentAction(null))} 
            aria-labelledby="simple-dialog-title" 
            open={currentAction === MANAGING_MAP}
        >
            <DialogTitle id="simple-dialog-title">Select a travel...</DialogTitle>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="select-travel">Travel</InputLabel>
                { /* TODO: Sacar el value 1 de ahi, tal vez con la data de usuario logeado se puede cargar el mapa por default */ }
                <Select
                    labelId="select-travel"
                    value={1}
                    onChange={handleSelectTravel}
                    label="Age"
                    ref={register}
                    fullWidth
                >
                    {
                        availablesTravels.map(item => 
                            <MenuItem key={item.uuid} value={item.uuid}>{item.name}</MenuItem>
                        )
                    }
                </Select>
            </FormControl>
            <DialogTitle id="simple-dialog-title">...or create a new one...</DialogTitle>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl variant="outlined" className={classes.formControl}>
                    <TextField
                        name="name"
                        label="Name"
                        required
                        inputRef={register}
                        variant="outlined"
                        size="small"
                    />
                </FormControl>
                <FormControl variant="outlined" className={classes.formControl}>
                    <Button variant="contained" color="primary" type="submit" value="Create">Create</Button>
                </FormControl>
            </form>
        </Dialog>
    )
}

export default ManageTravelDialog;