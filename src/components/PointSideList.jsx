import React, { Fragment } from 'react';
import {
    ListItem, ListItemIcon, ListItemText
} from '@material-ui/core'
import RoomIcon from '@material-ui/icons/Room';

import { useDispatch, useSelector } from 'react-redux';
import { setActivePoint } from '../redux/travelDuck';


const PointSideList = () => {

    const points = useSelector(store => store.travel.features.pointsArray);

    const dispatch = useDispatch();

    return (
        <Fragment>
            {
                points.map(item => (
                    <ListItem button onClick={() => dispatch(setActivePoint(item))}>
                        <ListItemIcon>
                            <RoomIcon />
                        </ListItemIcon>
                        <ListItemText primary={item.properties.name}/>
                    </ListItem>
                ))
            }
        </Fragment>
    );
}

export default PointSideList;