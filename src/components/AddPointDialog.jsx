import React from 'react';
import { 
    Button,
    DialogTitle,
    Dialog,
    FormControl,
    TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { addPoint } from '../redux/travelDuck';

const styles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    formControlFullWidth: {
        margin: theme.spacing(1),
        minWidth: 120,
        width: `calc(100% - 2 * ${theme.spacing(1)}px)`,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}))

const AddPointDialog = (props) => {

    const classes = styles();

    const newCoordinates = useSelector(store => store.travel.newCoordinates);
    const activeTravelUUID = useSelector(store => store.travel.travelUUID);
    const { register, handleSubmit } = useForm();
    const dispatch = useDispatch();
    const onSubmit = data => {
        dispatch(addPoint(data, activeTravelUUID));
        props.handleOnClose()
    }

    const handleOnClose = () => {
        props.handleOnClose()
    }


    return (
        <Dialog onClose={handleOnClose} aria-labelledby="simple-dialog-title" open={props.open}>
            <DialogTitle id="simple-dialog-title">Add new marker</DialogTitle>
            <form onSubmit={handleSubmit(onSubmit)}>

            <FormControl variant="outlined" className={classes.formControl}>
                <TextField
                    name="lat"
                    label="Latitude"
                    type="number" 
                    defaultValue={newCoordinates && newCoordinates.lat} 
                    inputRef={register}
                    variant="outlined"
                    size="small"
                />
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <TextField
                    name="lng"
                    label="Longitude"
                    type="number" 
                    defaultValue={newCoordinates && newCoordinates.lng} 
                    inputRef={register}
                    variant="outlined"
                    size="small"
                />
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <TextField
                    name="name"
                    label="Name"
                    inputRef={register({ required: true })}
                    variant="outlined"
                />
            </FormControl>
            <FormControl variant="outlined" className={classes.formControlFullWidth}>
                <TextField
                    name="description"
                    label="Description"
                    inputRef={register({ required: true })}
                    variant="outlined"
                    
                />
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <Button variant="contained" color="primary" type="submit" value="Add">Add</Button>
            </FormControl>
            </form>            
        </Dialog>
    )
}

export default AddPointDialog;