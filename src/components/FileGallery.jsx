import React from 'react';
import FileCard from './FileCard';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Box,
    IconButton,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { DropzoneDialog } from 'material-ui-dropzone';
import { useDispatch } from 'react-redux';
import { uploadPointFile } from '../redux/travelDuck';


const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      minWidth: 110,
      width: '100%',
    },
    image: {
      position: 'relative',
      minWidth: 110,
      minHeight: 158,
      [theme.breakpoints.down('xs')]: {
        width: '100% !important', // Overrides inline-style
        height: 100,
      },
      '&:hover, &$focusVisible': {
        zIndex: 1,
      },
    },
    focusVisible: {},
  }));

const FileGallery = (props) => {
    const { point, files } = props;
    const classes = useStyles();
    const dispatch = useDispatch();

    const [openDropzone, setOpenDropzone] = React.useState(false);

    const handleOpenDropzone = (state) => {
        setOpenDropzone(state);
    }

    const handleSaveDropzone = (files) => {
        files.forEach(file => {
            dispatch(uploadPointFile(point, file));
        });
        setOpenDropzone(false);
    }

    return (
        <Box
            display="flex"
            flexWrap="wrap"
            p={1}
            m={1}
            bgcolor="background.paper"
            css={{ maxWidth: '100%' }}
        >      
            {
                files.map(file => (
                    <Box key={file.id} p={1}>
                        <FileCard key={file.id} file={file}/>
                    </Box>
                ))
            }
            <Box p={1}>
                <IconButton 
                    aria-label="add-file" 
                    onClick={ () => handleOpenDropzone(true) }
                    focusRipple
                    className={classes.image}
                    focusVisibleClassName={classes.focusVisible}
                >
                    <AddIcon fontSize="large" />
                </IconButton>
            </Box>
            <DropzoneDialog
                open={openDropzone}
                onSave={handleSaveDropzone}
                acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
                showPreviews={true}
                maxFileSize={10*2**20}
                onClose={() => handleOpenDropzone(false)}
            />
        </Box>);
}


export default FileGallery;
