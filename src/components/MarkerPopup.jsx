import React from 'react'
import { Popup } from 'react-leaflet'
import { useDispatch } from 'react-redux';
import {
    IconButton,
    Typography,
} from '@material-ui/core';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import { setActivePoint } from '../redux/travelDuck';


const MarkerPopup = ({ point, handleMarkerDetails }) => {

    const dispatch = useDispatch();

    return (
        <Popup 
            position={[
                point.geometry.coordinates[1],
                point.geometry.coordinates[0]
            ]}
            onClose={() => {
              dispatch(setActivePoint(null))
            }}
        >
            <Typography gutterBottom variant="h5" component="h2">
                {point.properties.name}
            </Typography>
            <Typography variant="body2" gutterBottom>
                {point.properties.description}
            </Typography>
            <IconButton aria-label="details" onClick={ handleMarkerDetails }>
                <OpenInNewIcon fontSize="small"/>
            </IconButton>
        </Popup>
    )
}

export default MarkerPopup;