import React from 'react'
import { 
    Divider,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import PointDetailSide from './PointDetailSide';
import { useSelector, useDispatch } from 'react-redux';
import { setShowMarkerDrawer } from '../redux/coreDuck';

const drawerWidth = "50%"
const styles = makeStyles(theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    listDrawer: {
        paddingTop: 0
    }
}))
const MarkerSideDrawer = (props) => {

    const classes = styles();

    const dispatch = useDispatch();

    const activePoint = useSelector(store => store.travel.activePoint);

    return (
        <Drawer 
            className={classes.drawer}
            variant={props.variant}
            classes={{
                paper: classes.drawerPaper
            }} 
            open={props.open}
            onClose={() => props.onClose()}
            anchor="right"
        >
            <List component="nav" className={classes.listDrawer}>
                <ListItem className={classes.toolbar}>
                    <ListItemIcon onClick={() => dispatch(setShowMarkerDrawer(false))}>
                        <DoubleArrowIcon />
                    </ListItemIcon>
                    <ListItemText primary={activePoint && activePoint.properties.name}/>
                </ListItem>
                <Divider />
                { activePoint && (<PointDetailSide point={activePoint} />)}
            </List>
        </Drawer>
    );
}

export default MarkerSideDrawer;