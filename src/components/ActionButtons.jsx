import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import LinearScaleIcon from '@material-ui/icons/LinearScale';
import PlaceIcon from '@material-ui/icons/Place';
import MapIcon from '@material-ui/icons/Map';
import { useDispatch } from 'react-redux';

import { setCurrentAction } from '../redux/coreDuck'
import { ADDING_POINT, MANAGING_MAP, ADDING_PATH } from '../redux/coreDuck'

const actions = [
    { icon: <PlaceIcon />, name: 'Add marker', action: ADDING_POINT },
    { icon: <LinearScaleIcon />, name: 'Add path', action: ADDING_PATH },
    { icon: <MapIcon />, name: 'Manage maps', action: MANAGING_MAP },
];

const styles = makeStyles(theme => ({
    actionButtonWrapper: {
        position: 'relative',
        marginTop: theme.spacing(3),
        height: "95vh",
    },
    speedDial: {
        position: 'absolute',
        '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
          bottom: theme.spacing(2),
          right: theme.spacing(2),
        },
        '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
          top: theme.spacing(2),
          left: theme.spacing(2),
        },
      },
}))

const ActionButtons = () => {
    const classes = styles();
    const dispatch = useDispatch();


    const [open, setOpen] = React.useState(false);
    const handleClose = () => setOpen(false);
    const handleOpen = () => setOpen(true);

    const handleAction = (action) => {
        dispatch(setCurrentAction(action));
        handleClose();
    }

    return(
        <div className={classes.actionButtonWrapper}>
        <SpeedDial
            ariaLabel="SpeedDial example"
            className={classes.speedDial}
            icon={<SpeedDialIcon />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
            direction="up"
            >
            {actions.map((action) => (
                <SpeedDialAction
                    key={action.name}
                    icon={action.icon}
                    tooltipTitle={action.name}
                    onClick={() => handleAction(action.action)}
                />
            ))}
        </SpeedDial>
        </div>
    )
}

export default ActionButtons;