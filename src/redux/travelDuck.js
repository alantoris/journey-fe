// constantes 
const DEFAULT_COORDINATES = [-75.546518086577947, 45.467134581917357]

const initialData = {
    travelUUID: null,
    name: "",
    extent: [],
    centroid_farest_distance: null,
    features: {
        pointsArray : [],
        pathsArray: [],
    },
    activePoint: null,
    activePointFiles: [],
    mapCoordinates: DEFAULT_COORDINATES,
    newCoordinates: null,
    availablesTravels: [],
}

const GET_TRAVEL = "GET_TRAVEL";
const ADD_POINT = "ADD_POINT";
const CREATE_TAVEL = "CREATE_TAVEL";
const GET_TRAVEL_FEATURES = "GET_TRAVEL_FEATURES";
const SELECT_TRAVEL = "SELECT_TRAVEL";
const DELETE_POINT = "DELETE_POINT";
const SET_ACTIVE_POINT = "SET_ACTIVE_POINT";
const SET_NEW_COORDINATES = "SET_NEW_COORDINATES";
const GET_AVAILABLES_TRAVELS = "GET_AVAILABLES_TRAVELS";
const SELECT_DEFAULT_IF_NULL = "SELECT_DEFAULT_IF_NULL";
const UPLOAD_POINT_FILE = "UPLOAD_POINT_FILE";
const GET_ACTIVE_POINT_FILES = 'GET_ACTIVE_POINT_FILES';


// reducer
export default function travelReducer(state = initialData, action){
    switch(action.type){
        case GET_TRAVEL:
            return { 
                ...state, 
                travelUUID: action.payload.travelUUID,
                features: action.payload.features
            }
        case ADD_POINT:
            return {
                ...state, 
                features: {
                    pointsArray: [...state.features.pointsArray, action.payload],
                    pathsArray: state.features.pathsArray
                }
            }
        case CREATE_TAVEL:
            return {
                ...state,
                travelUUID: action.payload.uuid,
                name: action.payload.name,
            }
        case GET_TRAVEL_FEATURES:
            return {
                ...state,
                travelUUID: action.payload.uuid,
                name: action.payload.name,
                extent: action.payload.extent,
                mapCoordinates: action.payload.centroid ? action.payload.centroid : DEFAULT_COORDINATES,
                centroid_farest_distance: action.payload.farest_distance,
                features: action.payload.features
            }
        case SELECT_TRAVEL:
            return {
                ...state,
                travelUUID: action.payload,
            }
        case DELETE_POINT:
            return {
                ...state,
                features: {
                    ...state.features,
                    pointsArray: state.features.pointsArray.filter(point => point.id !== action.payload)
                }
            }
        case SET_ACTIVE_POINT:
            return { 
                ...state, 
                activePoint: action.payload,
                mapCoordinates: action.payload ? action.payload.geometry.coordinates : state.mapCoordinates
            }
        case SET_NEW_COORDINATES:
            return { 
                ...state, 
                newCoordinates: action.payload
            }
        case GET_AVAILABLES_TRAVELS:
            return {
                ...state,
                availablesTravels: action.payload
            }
        case SELECT_DEFAULT_IF_NULL:
            if (state.travelUUID === null)
                return {
                    ...state,
                    travelUUID: action.payload,
                }
            else
                return state
        case UPLOAD_POINT_FILE:
            return {
                ...state,
                activePointFiles: [ ...state.activePointFiles, action.payload ]
            }
        case GET_ACTIVE_POINT_FILES:
            return {
                ...state,
                activePointFiles: action.payload
            }
        default:
            return state
    }
}

//actions
export const selectTravel = (uuid) => (dispatch, getState) => {
    dispatch({
        type: SELECT_TRAVEL,
        payload: uuid
    });
}


export const getTravelFeatures = (uuid) => async (dispatch, getState) => {
    try{
        const requestOptions = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
        };
        fetch(`http://localhost:8000/travels/${uuid}/`, requestOptions)
        .then(response => response.json())
        .then(data => {
            let payload = data;
            console.log(data);
            payload['features'] = {}
            fetch(`http://localhost:8000/travels/${uuid}/points`, requestOptions)
            .then(
                response => response.json()
            )
            .then(
                data => {
                    payload['features']['pointsArray'] = data.features;
                    fetch(`http://localhost:8000/travels/${uuid}/connections`, requestOptions)
                        .then(
                            response => response.json()
                        )
                        .then(
                            data => {
                                payload['features']['pathsArray'] = data.features;
                                dispatch({
                                    type: GET_TRAVEL_FEATURES,
                                    payload: payload
                                });
                            }
                        );
                }
            );
        })
    } catch (error) { 
        console.log(error)
    }
}


export const addPoint = (point, uuid) =>  async (dispatch, getState) => {
    try{
        const requestOptions = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({ 
                name: point.name,
                description: point.description,
                travel: uuid,
                location: `POINT (${point.lng} ${point.lat})`
            }),
        };
        fetch(`http://localhost:8000/travels/${uuid}/points/`, requestOptions)
            .then(response => response.json())
            .then(data => {
                        console.log(data);
                        dispatch({
                            type: ADD_POINT,
                            payload: data
                        }) 
                    }
            );        
    } catch (error){
        console.log(error);
    }
}

export const createTravel = (data) =>  async (dispatch, getState) => {
    try{
        const requestOptions = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({ name: data.name }),
        };
        fetch('http://localhost:8000/travels/', requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                dispatch({
                    type: CREATE_TAVEL,
                    payload: data,
                })
                }
            )
    } catch (error){
        console.log(error);
    }
}

export const updatePoint = (point, body) =>  async (dispatch, getState) => {
    try{
        const requestOptions = {
            method: 'PATCH',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(body),
        };
        fetch(`http://localhost:8000/travels/${point.properties.travel}/points/${point.id}/`, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log(data); 
            });        
    } catch (error){
        console.log(error);
    }
}


export const deletePoint = (point) =>  async (dispatch, getState) => {
    try{
        const requestOptions = {
            method: 'DELETE',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
        };
        fetch(`http://localhost:8000/travels/${point.properties.travel}/points/${point.id}/`, requestOptions)
            .then(response => {response.json()})
            .then((data) => {
                dispatch({
                    type: DELETE_POINT,
                    payload: point.id
                }) 
            });  
    } catch (error){
        console.log(error);
    }      
}


export const setActivePoint = (point) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_ACTIVE_POINT,
            payload: point
        });
    } catch (error){
        console.log(error);
    }
}

export const setNewCoordinates = (coordinates) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_NEW_COORDINATES,
            payload: coordinates
        });
    } catch (error){
        console.log(error);
    }
}

export const getAvailablesTravels = () => async (dispatch, getState) => {
    try{
        const requestOptions = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
        };
        fetch('http://localhost:8000/travels/', requestOptions)
            .then(
                response => response.json()
            )
            .then(
                data => {
                    dispatch({
                        type: GET_AVAILABLES_TRAVELS,
                        payload: data
                    })
                    data.forEach(travel => {
                        //TODO: Asegurarse por bakend que solo haya uno
                        if(travel.default){
                            dispatch({
                                type: SELECT_DEFAULT_IF_NULL,
                                payload: travel.uuid
                            })
                        }
                    });
                    
                }
            );
    } catch(error){
        console.log(error)
    }
}

//TODO: Usar http://localhost:8000/travels/${point.properties.travel}/files  para obtener todas las files del travel
export const getPointFiles = (point) => async (dispatch, getState) => {
    try{
        const requestOptions = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
        };
        fetch(`http://localhost:8000/travels/${point.properties.travel}/points/${point.id}/files`, requestOptions)
            .then(
                response => response.json()
            )
            .then(
                data => {
                    dispatch({
                        type: GET_ACTIVE_POINT_FILES,
                        payload: data
                    })                    
                }
            );
    } catch(error){
        console.log(error)
    }
}


export const clearActivePointFiles = () => async (dispatch, getState) => {
    try{
        dispatch({
            type: GET_ACTIVE_POINT_FILES,
            payload: []
        })                    
    } catch(error){
        console.log(error)
    }
}


export const uploadPointFile = (point, file) => async (dispatch, getState) => {
    try{
        const requestOptions = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
            body: file,
        };
        fetch(`http://localhost:8000/travels/${point.properties.travel}/points/${point.id}/file/${file.name}`, requestOptions)
            .then(
                response => response.json()
            )
            .then(
                data => {
                    dispatch({
                        type: UPLOAD_POINT_FILE,
                        payload: data
                    })
                }
            );
    } catch(error){
        console.log(error)
    }
}