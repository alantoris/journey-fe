const initialData = {
    token: localStorage.getItem('token') || null,
    user: null,
}
//TODO: check is localStorage if the best way to storage a token
const SET_TOKEN = "SET_TOKEN";
const SET_USER = "SET_USER";

export default function userReducer(state = initialData, action){
    switch(action.type){
        case SET_TOKEN:
            return { 
                ...state, 
                token: action.payload,
            }
        case SET_USER:
            return {
                ...state,
                user: action.payload,
            }
        default:
            return state
    }
}

export const signIn = (token) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_TOKEN,
            payload: token
        });
    } catch (error){
        console.log(error);
    }
}

export const logOut = () => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_TOKEN,
            payload: null
        });
    } catch (error){
        console.log(error);
    }
}

export const getUserData = () => async (dispatch, getState) => {
    try{
        const requestOptions = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Token ${localStorage.getItem('token')}`,
            },
        };
        fetch(`http://localhost:8000/users/0/`, requestOptions)
            .then(response => response.json())
            .then(data => {
                dispatch({
                    type: SET_TOKEN,
                    payload: data
                });
            }); 
    } catch (error){
        console.log(error);
    }
}
    