import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import travelReducer from './travelDuck';
import coreReducer from './coreDuck';
import userReducer from './userDuck';

const rootReducer = combineReducers({
    travel: travelReducer,
    core: coreReducer,
    user: userReducer,
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk)) )
    return store;
}