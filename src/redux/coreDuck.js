// constantes 
export const ADDING_POINT = "ADDING_POINT"
export const MANAGING_MAP = "MANAGING_MAP"
export const ADDING_PATH = "ADDING_PATH"

const DEFAULT_MAX_BOUNDS = [[84.712, -174.227], [-84.774, 220.125]]
/*
test de initial_zoom
   centroid_farest_distance: 85 -> zoom: 2
*/
//TODO: Calcular el zoom inicial, en base a `centroid_farest_distance` de travelDuck
const INITIAL_ZOOM = 2
const MAX_ZOOM = 15
const MIN_ZOOM = 2

const initialData = {
    showDrawer: false,
    showMarkerDrawer: false,
    currentAction: null,
    mapInitialData: {
        zoom: INITIAL_ZOOM,
        bounds: DEFAULT_MAX_BOUNDS,
        maxZoom: MAX_ZOOM,
        minZoom: MIN_ZOOM,
    }    
}

const SET_SHOW_DRAWAER = "SET_SHOW_DRAWAER";
const SET_CURRENT_ACTION = "SET_CURRENT_ACTION";
const SET_SHOW_MARKER_DRAWAER = "SET_SHOW_MARKER_DRAWAER";


// reducer
export default function coreReducer(state = initialData, action){
    switch(action.type){
        case SET_SHOW_DRAWAER:
            return { 
                ...state, 
                showDrawer: action.payload
            }
        case SET_CURRENT_ACTION:
            return { 
                ...state, 
                currentAction: action.payload
            }
        case SET_SHOW_MARKER_DRAWAER:
            return { 
                ...state, 
                showMarkerDrawer: action.payload
            }
        default:
            return state
    }
}

// acciones
export const setShowDrawer = (showDrawer) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_SHOW_DRAWAER,
            payload: showDrawer
        });
    } catch (error){
        console.log(error);
    }
}

export const setCurrentAction = (currentAction) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_CURRENT_ACTION,
            payload: currentAction
        });
    } catch (error){
        console.log(error);
    }
}

export const setShowMarkerDrawer = (showMarkerDrawer) => async (dispatch, getState) => {
    try{
        dispatch({
            type: SET_SHOW_MARKER_DRAWAER,
            payload: showMarkerDrawer
        });
    } catch (error){
        console.log(error);
    }
}
