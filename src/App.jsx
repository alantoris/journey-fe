import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles'
import theme from './themeConfig'
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from 'react-router-dom';
import Container from './components/Container'
import AccountManager from './components/account/AccountManager'
import { Provider } from 'react-redux';
import generateStore from './redux/store';


function App() {
  const store = generateStore();

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/travel"  component={Container} />
            <Route path="/account" component={AccountManager} />
            <Redirect from="/" to="/account" />
          </Switch>
        </Router>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
